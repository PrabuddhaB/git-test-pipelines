package ellentube.infrastructure;

public class ConfigurationManager {
    private static ConfigurationManager instance = null;

    private static final String TEST_BROWSER = "testBrowser";

    private ConfigurationManager() { }

    public static ConfigurationManager getInstance() {
        if (instance == null) {
            return new ConfigurationManager();
        }

        return instance;
    }

    public String getTestBrowser() {
        return getEnvironmentVariable(TEST_BROWSER, "chrome");
    }

    private String getEnvironmentVariable(String variable, String defaultValue) {
        return System.getenv(variable) != null? System.getenv(variable) : defaultValue;
    }
}

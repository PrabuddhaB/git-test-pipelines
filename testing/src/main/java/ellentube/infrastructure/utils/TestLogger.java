package ellentube.infrastructure.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TestLogger {

    private int step = 1;

    public void log(String message) {
        String output = step +") " + timeNow() + " [" + currentProcess() + "]: " + message;
        System.out.println(output);
        step++;
    }

    private String timeNow() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS");
        return sdf.format(date);
    }

    private String currentProcess() {
        return Thread.currentThread().getName();
    }
}

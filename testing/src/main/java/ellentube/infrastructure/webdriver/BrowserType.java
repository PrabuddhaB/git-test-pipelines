package ellentube.infrastructure.webdriver;

public enum BrowserType {
    CHROME,
    FIREFOX;

    public static BrowserType fromString (String value) {
        return valueOf(value.toUpperCase());
    }


}

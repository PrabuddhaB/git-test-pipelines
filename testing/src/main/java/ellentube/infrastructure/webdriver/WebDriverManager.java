package ellentube.infrastructure.webdriver;

import ellentube.infrastructure.ConfigurationManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class WebDriverManager {
    public static WebDriver create() {
        BrowserType type = BrowserType.fromString(ConfigurationManager.getInstance().getTestBrowser());
        WebDriver driver;

        switch(type) {
            case CHROME:
                driver = new ChromeDriver();
                break;
            case FIREFOX:
                driver = new FirefoxDriver();
                break;
            default:
                throw new RuntimeException("Such browser is not supported");
        }

        return driver;
    }

    public static void destroy(WebDriver driver) {
        if (driver != null) {
            driver.quit();
        }
    }
}

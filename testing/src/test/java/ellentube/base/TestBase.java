package ellentube.base;

import ellentube.drivers.Application;
import ellentube.infrastructure.utils.TestLogger;
import ellentube.infrastructure.webdriver.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;

public class TestBase {
    protected WebDriver driver;
    protected TestLogger testLogger;
    protected Application app;

    @Before
    public void setUp() {
        driver = WebDriverManager.create();

        app = new Application(driver);

        beforeTest();
    }

    @After
    public void cleanUp() {
        afterTest();

        WebDriverManager.destroy(driver);
    }

    protected void beforeTest() {
        driver.get("https://www.ellentube.com/");
    }

    protected void afterTest() {
    }
}
